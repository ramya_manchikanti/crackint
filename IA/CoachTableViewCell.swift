//
//  CoachTableViewCell.swift
//  IA
//
//  Created by Rhiya Manchikanti on 6/28/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//

import UIKit

class CoachTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBOutlet weak var mockInterviewExperience: UITextView!
    @IBOutlet weak var hourlyRate: UITextField!

}
