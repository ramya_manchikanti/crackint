//
//  BuddyFeedTableViewCell.swift
//  InterviewAssist
//
//  Created by Ranjani  Manchikanti on 4/22/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//

import UIKit

class BuddyFeedTableViewCell: PFTableViewCell {


    @IBOutlet weak var postTitle: UILabel!
    
    @IBOutlet weak var lookingForLabel: UILabel!
    @IBOutlet weak var requiredProfession: UITextView!
    
    
    @IBOutlet weak var buddyFeedBottomBarImage: UIImageView!
    
    @IBOutlet weak var buddyFeedNameLabel: UILabel!
    @IBOutlet weak var buddyFeedProfessionLabel: UILabel!
    
    @IBOutlet weak var AreaOfExpertiseLabel: UILabel!
    @IBOutlet weak var skillSetListTextView: UITextView!
    
    @IBOutlet weak var experienceLevelLabel: UILabel!
    @IBOutlet weak var requiredExperienceLevelLabel: UILabel!


    @IBOutlet weak var buddyFeedProfileImage: UIImageView!
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        var postTitleFont = "AvenirNext-DemiBold"
        var labelFont = "AvenirNext-Medium"
        var resultFont = "AvenirNext-Regular"
        
        self.postTitle.font = UIFont(name: postTitleFont, size:16.0)
        self.postTitle.text = "Post Title"
        
//        self.lookingForLabel.font = UIFont(name: labelFont, size:15.0)
//        self.lookingForLabel.textColor = UIColor.grayColor()
//        self.lookingForLabel.text = "Looking for: "
//        
//        self.requiredProfession.font = UIFont(name: resultFont, size:13.0)
//        self.requiredProfession.text = "Post Doc"
//        self.requiredProfession.textColor = UIColor.grayColor()
        
        self.requiredExperienceLevelLabel.font = UIFont(name: labelFont, size:15.0)
        self.requiredExperienceLevelLabel.textColor = UIColor.grayColor()
        self.requiredExperienceLevelLabel.text = "Required experience level:"
        
        self.experienceLevelLabel.font = UIFont(name: resultFont, size:14.0)
        self.experienceLevelLabel.textColor = UIColor.grayColor()
        self.experienceLevelLabel.text = "1-5 yrs"
        
        self.AreaOfExpertiseLabel.font = UIFont(name: labelFont, size:15.0)
        self.AreaOfExpertiseLabel.textColor = UIColor.grayColor()
        self.AreaOfExpertiseLabel.text = "Required skill set:"
        
        self.skillSetListTextView.font = UIFont(name: resultFont, size:14.0)
        self.skillSetListTextView.textColor = UIColor.grayColor()
        self.skillSetListTextView.text = "VLSI, Analog devices, MIPS, Chip Designing"
    
        self.buddyFeedBottomBarImage.backgroundColor = UIColor(red: 0.906, green: 0.055, blue: 0.282, alpha: 1
        )
       // var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light)) as UIVisualEffectView
       // visualEffectView.frame = buddyFeedBottomBarImage.bounds
       // buddyFeedBottomBarImage.addSubview(visualEffectView)
        
        
      //  self.buddyFeedBottomBarImage.backgroundColor = UIColor(red: 0.149, green: 0.192, blue: 0.251, alpha:0.8)
        
        self.buddyFeedProfileImage.layer.masksToBounds = true
        self.buddyFeedProfileImage.clipsToBounds = true
        self.buddyFeedProfileImage.layer.cornerRadius = 25
        self.buddyFeedProfileImage.layer.borderWidth = 0.8
        self.buddyFeedProfileImage.layer.borderColor = UIColor.whiteColor().CGColor;
        
        self.buddyFeedNameLabel.textColor = UIColor.whiteColor()
        self.buddyFeedNameLabel.text = "Thvishi Sriadibhatla"
        self.buddyFeedNameLabel.font = UIFont(name: labelFont, size:13.0)
        
        self.buddyFeedProfessionLabel.textColor = UIColor.whiteColor()
        self.buddyFeedProfessionLabel.text = "BITS-Pilani, USC, Oracle"
        self.buddyFeedProfessionLabel.font = UIFont(name: labelFont, size:13.0)
        
    
    }

}
