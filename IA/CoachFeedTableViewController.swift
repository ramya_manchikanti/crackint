//
//  CoachFeedTableViewController.swift
//  IA
//
//  Created by Ranjani  Manchikanti on 6/21/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//

import UIKit

class CoachFeedTableViewController: PFQueryTableViewController, UISearchResultsUpdating {
    
    var category:NSString = ""
    
    var searchResults = [PFObject]()
    
    var feedObject = PostObject()
    
    var resultSearchController = UISearchController()
    
    // Initialise the PFQueryTable tableview
    override init(style: UITableViewStyle, className: String!) {
        super.init(style: style, className: className)
    }
    
    override func viewWillAppear(animated: Bool) {
        // navigationItem.title = &quot;One&quot;
        navigationItem.title = "I.A"
        
        var font = UIFont(name: "AvenirNext-DemiBold", size: 20)
        var barTintColor = UIColor(red: 246/255, green: 250/255, blue: 249/255, alpha: 0.5)
        var tintColor = UIColor(red: 0.906, green: 0.055, blue: 0.282, alpha: 1)
        navigationController?.navigationBar.barTintColor = barTintColor
        navigationController?.navigationBar.tintColor = tintColor
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Configure the PFQueryTableView
        self.parseClassName = "UserProfile"
        //self.textKey = "nameEnglish"
        self.pullToRefreshEnabled = true
        self.paginationEnabled = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            
            self.tableView.tableHeaderView = controller.searchBar
            
            return controller
        })()
        
        self.tableView.reloadData()
        
        // Do any additional setup after loading the view
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func queryForTable() -> PFQuery {
        var query:PFQuery = PFQuery(className:self.parseClassName!)
        query.orderByDescending("createdAt")
        query.whereKey("Category", matchesRegex:self.category as String)
        //println("noof objects", objects?.count)
        
        //        if(objects?.count == 0)
        //        {
        //            query.cachePolicy = PFCachePolicy.CacheThenNetwork
        //        }
        
        //query.orderByAscending("Title")
        
        return query
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        
        if (self.resultSearchController.active)
        {
            println("Search Size2", self.searchResults.count)
            return self.searchResults.count
        }
        else
        {
            return self.objects!.count
        }
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath, object: PFObject?) -> PFTableViewCell? {
        
        let cellIdentifier:String = "CoachFeedTableViewCell"
        var cell:CoachFeedTableViewCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as? CoachFeedTableViewCell
        
        if(cell == nil) {
            cell = CoachFeedTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: cellIdentifier)
        }
        
        var final_object:PFObject
        println("Search Size1", self.searchResults.count)
        
        if (self.resultSearchController.active) {
            final_object = searchResults[indexPath.row]
            println("search title", final_object["Title"] as? String)
        } else {
            final_object = object!;
        }
        
        //var
        
        //cell?.coachName.text = final_object["firstName"] as? String + " " + final_object["firstName"]
        //cell?.experienceLevelLabel.text = final_object["ExperienceLevel"] as? String
        //cell?.skillSetListTextView.text = final_object["Skills"] as? String
        
        //cell?.buddyFeedProfessionLabel.text = final_object["Title"] as? String
        //cell?.buddyFeedProfessionLabel.text = "Test"
        
        //cell?.buddyFeedProfileImage?.image = UIImage(named:ImageName)
        
        return cell;
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        var object = PFObject(className:"InterviewPost")
        
        if segue.identifier == "BuddyDetailSegue" {
            if let destination = segue.destinationViewController as? BuddyDetailTableViewController {
                if (self.resultSearchController.active) {
                    if let row = self.searchDisplayController?.searchResultsTableView.indexPathForSelectedRow()?.row {
                        object = searchResults[row]
                    }
                }
                else {
                    if let row = tableView.indexPathForSelectedRow()?.row {
                        object = self.objects![row] as! PFObject
                    }
                }
                destination.detailObject.postTitle = object["Title"] as! String
                destination.detailObject.postSkills = object["Skills"] as! String
                destination.detailObject.postAddInfo = object["Description"] as! String
                destination.detailObject.postExp = object["ExperienceLevel"] as! String
            }
        }
    }
    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
    //SEARCH METHODS
    func updateSearchResultsForSearchController(searchController: UISearchController)
    {
        self.searchResults.removeAll(keepCapacity: false)
        let searchText = searchController.searchBar.text
        var object:PFObject
        var query:PFQuery = PFQuery(className: "InterviewPost")
        query.orderByDescending("createdAt")
        query.whereKey("Category", matchesRegex:self.category as String)
        query.whereKey("Search", matchesRegex:searchText)
        query.findObjectsInBackgroundWithBlock{ (objects,error) -> Void in
            if error == nil {
                self.searchResults.removeAll(keepCapacity: false)
                for object in objects! {
                    self.searchResults.append(object as! PFObject)
                }
            }
            println("Search Size", self.searchResults.count)
            self.tableView.reloadData()
        }
    }
    //    func filterContentForSearchText(searchText: String, scope: String = "Title")
    //
    //    {
    //        self.searchResults.removeAll(keepCapacity: false)
    //        var object:PFObject
    //        var query:PFQuery = PFQuery(className: "InterviewPost")
    //        query.orderByDescending("createdAt")
    //        query.whereKey("Category", matchesRegex:"Software")
    //        query.findObjectsInBackgroundWithBlock{ (objects,error) -> Void in
    //            if error == nil {
    //                for object in objects! {
    //                    self.searchResults.append(object as! PFObject)
    //                }
    //            }
    //            println("Search Size", self.searchResults.count)
    //            self.tableView.reloadData()
    //        }
    //    }
    //
    //    func searchDisplayController(controller: UISearchDisplayController, shouldReloadTableForSearchString searchString: String!) -> Bool
    //
    //    {
    //        self.filterContentForSearchText(searchString)
    //        return true
    //    }
    
    //    func searchDisplayController(controller: UISearchDisplayController, shouldReloadTableForSearchScope searchOption: Int) -> Bool
    //        
    //    {
    //        self.filterContentForSearchText(self.searchDisplayController!.searchBar.text)
    //        return true
    //    }
    
    
}
