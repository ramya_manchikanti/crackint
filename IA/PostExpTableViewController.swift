//
//  PostExpTableViewController.swift
//  InterviewAssist
//
//  Created by Ranjani  Manchikanti on 5/12/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//

import UIKit

protocol PostExpTableViewControllerDelegate
{
    func PostExpTableViewControllerDidBack(controller : PostExpTableViewController)
}

class PostExpTableViewController: UITableViewController, PostSkillsViewControllerDelegate {
    
    var delegate:PostExpTableViewControllerDelegate?
    
    var exp = ["Fresh Grad","1-5 years","5-10 years", ">10 years", "Doesn't matter"]
    
    var postObjectExp = PostObject()
    
    override func viewWillAppear(animated: Bool) {
        // navigationItem.title = &quot;One&quot;
        navigationItem.title = "I.A"
        
        var font = UIFont(name: "AvenirNext-DemiBold", size: 20)
        var barTintColor = UIColor(red: 209/255, green: 212/255, blue: 211/255, alpha: 1)
        var tintColor = UIColor(red: 0.906, green: 0.055, blue: 0.282, alpha: 1)
        navigationController?.navigationBar.barTintColor = barTintColor
        navigationController?.navigationBar.tintColor = tintColor
    }
    
    @IBAction func cancelButton(sender: AnyObject) {
        println("PostQuestion3ViewController Cancel Button")
        //Create the AlertController
        let actionSheetController: UIAlertController = UIAlertController(title: "Alert", message: "Are you sure you want to cancel this post?", preferredStyle: .Alert)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
            //Do some stuff
        }
        actionSheetController.addAction(cancelAction)
        //Create and an option action
        let oKAction: UIAlertAction = UIAlertAction(title: "OK", style: .Default) { action -> Void in
            self.performSegueWithIdentifier("ExpCancel", sender: self)
            //self.tabBarController?.selectedIndex = 2
        }
        actionSheetController.addAction(oKAction)
        //        //Add a text field
        //        actionSheetController.addTextFieldWithConfigurationHandler { textField -> Void in
        //            //TextField configuration
        //            textField.textColor = UIColor.blueColor()
        //        }
        //Present the AlertController
        self.presentViewController(actionSheetController, animated: true, completion: nil)
        //self.performSegueWithIdentifier("Q3ToPostSegue", sender: self)
    }
    @IBAction func backButton(sender: AnyObject) {
        println("PostExpTableViewController Back Button")
        self.delegate?.PostExpTableViewControllerDidBack(self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.tableView.backgroundColor = UIColor(red: 243.0/255, green: 243.0/255, blue: 243.0/255, alpha: 1)
        self.tableView.tableFooterView = UIView()
       // self.tableView.backgroundColor = UIColor.clearColor()
       // self.tableView.layoutMargins = UIEdgeInsetsZero
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
    
    }
    
    
    
    
    //only apply the blur if the user hasn't disabled transparency effectsif 
    
    //view is self.view in a UIViewControllerview.addSubview(blurEffectView)
    //if you have more UIViews on screen, use insertSubview:belowSubview: to place it underneath the lowest view
    //add auto layout constraints so that the blur fills the screen upon rotating deviceblurEffectView.setTranslatesAutoresizingMaskIntoConstraints(false)view.addConstraint(NSLayoutConstraint(item: blurEffectView, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0))view.addConstraint(NSLayoutConstraint(item: blurEffectView, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 0))view.addConstraint(NSLayoutConstraint(item: blurEffectView, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.Leading, multiplier: 1, constant: 0))view.addConstraint(NSLayoutConstraint(item: blurEffectView, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: view, attribute: NSLayoutAttribute.Trailing, multiplier: 1, constant: 0))} else {view.backgroundColor = UIColor.blackColor()}
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return self.exp.count
    }
    
    override func tableView( tableView : UITableView,  titleForHeaderInSection section: Int)->String {
        println("Entered here")
        switch(section) {
        case 0:
            return "Select required experience level"
        default :
            return "abc"
            
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        

        let cell = tableView.dequeueReusableCellWithIdentifier("PostExpTableViewCell", forIndexPath: indexPath) as! PostExpTableViewCell
        
        self.navigationController?.navigationBar.tintColor = UIColor(red: 0.906, green: 0.055, blue: 0.282, alpha: 1)
        
        var expLabelFont = "AvenirNext-Regular"
        cell.expLabel.font = UIFont(name: expLabelFont, size:15.0)
        cell.expLabel.text = self.exp[indexPath.row]
        //cell.backgroundColor = UIColor.clearColor()
        
      //  var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light)) as UIVisualEffectView
       // visualEffectView.frame = CGRectMake(0, 0, cell.bounds.width, cell.bounds.height)
        //cell.addSubview(visualEffectView)
        
        
        return cell
    }
    
    func PostExpTableViewControllerDidBack(controller: PostExpTableViewController)
    {
        println("PostExpTableViewController back delegate function")
        self.dismissViewControllerAnimated(true, completion:nil)
        //self.tabBarController?.selectedIndex = 0
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        
        if segue.identifier == "ExpToSkills" {
            if let destinationVC = segue.destinationViewController as? UINavigationController {
                println("Entered ExpToSkills")
                var skillsVC  = destinationVC.viewControllers[0] as? PostSkillsViewController
                skillsVC?.postObjectSkills = self.postObjectExp
                skillsVC?.delegate = self
            }
        }
    }
    
    func PostSkillsViewControllerDidBack(controller: PostSkillsViewController)
    {
        println("PostSkillsViewController back delegate function")
        self.dismissViewControllerAnimated(true, completion:nil)
        //self.tabBarController?.selectedIndex = 0
    }
    
    /*override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(section == 0) {
            var view = UIView() // The width will be the same as the cell, and the height should be set in tableView:heightForRowAtIndexPath:
            var label = UILabel()
            label.text="My Details"
          //  let views = ["label": label,"button":button,"view": view]
            view.addSubview(label)
            return view
        }
        return nil
    }*/
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    

    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell,
        forRowAtIndexPath indexPath: NSIndexPath)
    {
        // Remove separator inset
        if cell.respondsToSelector("setSeparatorInset:") {
            cell.separatorInset = UIEdgeInsetsZero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.respondsToSelector("setPreservesSuperviewLayoutMargins:") {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.respondsToSelector("setLayoutMargins:") {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }
    
  /*  override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        //Add vibrancy text effect
    
        tableView.backgroundColor = UIColor.clearColor()
        let blurEffect = UIBlurEffect(style: .Light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        tableView.backgroundView = blurEffectView
        return tableView.backgroundView
    }*/
    
    /*
    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let headerView = view as! InterviewAssist.PostExpTableViewCell
        headerView.textLabel!.textColor = UIColor(red: 151.0/255, green: 193.0/255, blue: 100.0/255, alpha: 1)
        let font = UIFont(name: "Montserrat", size: 18.0)
        headerView.textLabel!.font = font!
    } */
    

}
