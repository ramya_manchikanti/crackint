//
//  HomeTableViewController.swift
//  InterviewAssist
//
//  Created by Ranjani  Manchikanti on 4/19/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//

import UIKit

class HomeTableViewController: UITableViewController {

    var homeCells = ["Buddy", "Coach"]
    
    override func viewWillAppear(animated: Bool) {
        // navigationItem.title = &quot;One&quot;
        navigationItem.title = "I.A"
        
        var font = UIFont(name: "AvenirNext-DemiBold", size: 20)
        var barTintColor = UIColor(red: 246/255, green: 250/255, blue: 249/255, alpha: 0.5)
        var tintColor = UIColor(red: 0.906, green: 0.055, blue: 0.282, alpha: 1)
        navigationController?.navigationBar.barTintColor = barTintColor
        navigationController?.navigationBar.tintColor = tintColor
      //  [self.navigationController.navigationBar setTitleTextAttributes,:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor]];

            
        

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        tableView.backgroundView = nil
        tableView.layer.borderWidth = 0.5
        tableView.layer.cornerRadius = 0.5
        tableView.separatorStyle = .None;
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        return 2
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("HomeTableViewCell", forIndexPath: indexPath) as! HomeTableViewCell
        

        // Configure the cell...
        cell.cellTitle?.text = self.homeCells[indexPath.row]
       
        var ImageName = self.homeCells[indexPath.row] + ".jpg"
        cell.backgroundImage?.image = UIImage(named:ImageName)
       
        cell.selectionStyle = .None
        cell.backgroundColor = UIColor.clearColor()
        //cell.layoutMargins = UIEdgeInsets(top:0,left:0,bottom:146,right:0)
        
       
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Get the row data for the selected row
        if(indexPath.row==0){
            self.performSegueWithIdentifier("Categories1Segue", sender: self)
        }
        else if(indexPath.row==1){
            self.performSegueWithIdentifier("Categories2Segue", sender: self)
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "Categories1Segue" {
            if let destination = segue.destinationViewController as? CategoriesTableViewController {
                if let interview_type = tableView.indexPathForSelectedRow()?.row {
                    destination.mainCategory = "Buddy"
                    //destination.blogName = swiftBlogs[blogIndex]
                }
            }
        }
        else if segue.identifier == "Categories2Segue" {
            if let destination = segue.destinationViewController as? CategoriesTableViewController {
                if let interview_type = tableView.indexPathForSelectedRow()?.row {
                    destination.mainCategory = "Coach"
                    //destination.blogName = swiftBlogs[blogIndex]
                }
            }
        }


    }
    
}
