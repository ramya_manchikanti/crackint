//
//  PostCategoriesTableViewController.swift
//  InterviewAssist
//
//  Created by Ranjani  Manchikanti on 5/11/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//

import UIKit

//Categories->Title->Exp->Skills->AddInfo->Final

class PostCategoriesTableViewController: UITableViewController, LoginViewControllerDelegate, PostTitleViewControllerDelegate {
    
    var category = ["Banking","Legal","Software", "Hardware", "Manufacturing"]
    
    var postObjectCategory = PostObject()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.title = "I.A"
        
        var font = UIFont(name: "AvenirNext-DemiBold", size: 20)
        var barTintColor = UIColor(red: 246/255, green: 250/255, blue: 249/255, alpha: 0.5)
        var tintColor = UIColor(red: 0.906, green: 0.055, blue: 0.282, alpha: 1)
        navigationController?.navigationBar.barTintColor = barTintColor
        navigationController?.navigationBar.tintColor = tintColor
        
        if (PFUser.currentUser() == nil) {
            println("User is not logged in")
            self.performSegueWithIdentifier("PostLoginViewSegue", sender: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        //return (1 + self.category.count)
        return self.category.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
//        if (indexPath.row == 0) {
//            let cell = tableView.dequeueReusableCellWithIdentifier("PostTitleTableViewCell", forIndexPath: indexPath) as! PostTitleTableViewCell
//            cell.titleLabel.text = "Looking for a interview buddy"
//            return cell
//        }
//        else {
            let cell = tableView.dequeueReusableCellWithIdentifier("PostCategoriesTableViewCell", forIndexPath: indexPath) as! PostCategoriesTableViewCell
     //       cell.categoriesLabel.text = self.category[indexPath.row - 1]
            cell.categoriesLabel.text = self.category[indexPath.row]
        
            var cetegoriesLabelFont = "AvenirNext-Regular"
            cell.categoriesLabel.font = UIFont(name: cetegoriesLabelFont, size:15.0)
        
        //cell.backgroundColor = UIColor.clearColor()
            return cell
//        }
        
    }
    
    override func tableView( tableView : UITableView,  titleForHeaderInSection section: Int)->String {
        println("Entered here")
        switch(section) {
        case 0:
            return "Select the category"
        default :
            return "abc"
            
        }
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell,
        forRowAtIndexPath indexPath: NSIndexPath)
    {
        // Remove separator inset
        if cell.respondsToSelector("setSeparatorInset:") {
            cell.separatorInset = UIEdgeInsetsZero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.respondsToSelector("setPreservesSuperviewLayoutMargins:") {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.respondsToSelector("setLayoutMargins:") {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        println("Entered PostExpSegue")
        
        if segue.identifier == "PostLoginViewSegue" {
            if let destinationVC = segue.destinationViewController as? UINavigationController {
                println("Entered PostLoginViewSegue")
                var loginVC  = destinationVC.viewControllers[0] as? LoginViewController
                loginVC?.sourceController = "Login"
                loginVC?.delegate = self
            }
        }
        else if segue.identifier == "CategoriesToTitle" {
            if let destinationVC = segue.destinationViewController as? UINavigationController {
                println("Entered PostTitleSegue")
                var titleVC  = destinationVC.viewControllers[0] as? PostTitleViewController
                titleVC?.delegate = self
                titleVC?.postObjectTitle = self.postObjectCategory
            }
        }
    }
    
    func UserLoginViewControllerDidCancel(controller: LoginViewController)
    {
        println("Post login cancel delegate function")
        self.dismissViewControllerAnimated(true, completion:nil)
        self.tabBarController?.selectedIndex = 0
    }
    
    func UserLoginViewControllerDidLogin(controller: LoginViewController)
    {
        println("Post login did login delegate function")
        self.dismissViewControllerAnimated(true, completion:nil)
        self.tabBarController?.selectedIndex = 2
    }
    
    func PostTitleViewControllerDidBack(controller: PostTitleViewController)
    {
        println("PostTitleViewController back delegate function")
        self.dismissViewControllerAnimated(true, completion:nil)
        //self.tabBarController?.selectedIndex = 0
    }

}
