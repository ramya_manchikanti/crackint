//
//  BasicProfileTableViewCell.swift
//  IA
//
//  Created by Rhiya Manchikanti on 6/27/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//


class BasicProfileTableViewCell: UITableViewCell {
    
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    
    @IBOutlet weak var lastNameTextField: UITextField!

    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var lastNameLabel: UILabel!
    
    @IBOutlet weak var firstNameLabel: UILabel!
    
    
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var phoneLabel: UILabel!
    
    @IBOutlet weak var coachQuestionLabel: UILabel!
    
    @IBOutlet weak var coachAnswer_Outlet: UISwitch!

    @IBAction func coachAnswer_Action(sender: AnyObject) {
        
        
        
    }

}
