//
//  PostSkillsViewController.swift
//  InterviewAssist
//
//  Created by Ranjani  Manchikanti on 5/2/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//

import UIKit

protocol PostSkillsViewControllerDelegate
{
    func PostSkillsViewControllerDidBack(controller : PostSkillsViewController)
}

class PostSkillsViewController: UIViewController, UITextViewDelegate, PostAddInfoViewControllerDelegate {
    
    var delegate:PostSkillsViewControllerDelegate?
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var skillSetTitle: UILabel!
    @IBOutlet weak var skillSetTextView: UITextView!
    
    var postObjectSkills = PostObject()

    override func viewWillAppear(animated: Bool) {
        // navigationItem.title = &quot;One&quot;
        navigationItem.title = "I.A"
        
        var font = UIFont(name: "AvenirNext-DemiBold", size: 20)
        var barTintColor = UIColor(red: 209/255, green: 212/255, blue: 211/255, alpha: 1)
        var tintColor = UIColor(red: 0.906, green: 0.055, blue: 0.282, alpha: 1)
        navigationController?.navigationBar.barTintColor = barTintColor
        navigationController?.navigationBar.tintColor = tintColor
    }
    
    @IBAction func cancelButton(sender: AnyObject) {
        println("PostSkillsViewController Cancel Button")
        //Create the AlertController
        let actionSheetController: UIAlertController = UIAlertController(title: "Alert", message: "Are you sure you want to cancel this post?", preferredStyle: .Alert)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
            //Do some stuff
        }
        actionSheetController.addAction(cancelAction)
        //Create and an option action
        let oKAction: UIAlertAction = UIAlertAction(title: "OK", style: .Default) { action -> Void in
            self.performSegueWithIdentifier("SkillsCancel", sender: self)
            //self.tabBarController?.selectedIndex = 2
        }
        actionSheetController.addAction(oKAction)
//        //Add a text field
//        actionSheetController.addTextFieldWithConfigurationHandler { textField -> Void in
//            //TextField configuration
//            textField.textColor = UIColor.blueColor()
//        }
        //Present the AlertController
        self.presentViewController(actionSheetController, animated: true, completion: nil)
        //self.performSegueWithIdentifier("Q3ToPostSegue", sender: self)
    }
    @IBAction func backButton(sender: AnyObject) {
        println("PostSkillsViewController Back Button")
        self.delegate?.PostSkillsViewControllerDidBack(self)
    }
    @IBOutlet weak var textViewQuestion3: UITextView!
    var currentResponder:AnyObject = "Nil"
    
    var postObjectQ3 = PostObject()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.skillSetTextView.delegate = self
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("tapDetector"))
        self.view.addGestureRecognizer(gestureRecognizer)
     //   self.skillSetTextView.text = "Please enter a brief description"
        self.skillSetTextView.textColor = UIColor.lightGrayColor()
        
        self.navigationController?.navigationBar.tintColor = UIColor(red: 0.906, green: 0.055, blue: 0.282, alpha: 1)
        
        var postTitleFont1 = "AvenirNext-Regular"
        var examplesFont = "AvenirNext-UltraLightItalic"
        
        self.skillSetTitle.font = UIFont(name: postTitleFont1, size:20)
        self.skillSetTitle.text = "Skill set"
        
        self.nextButton.backgroundColor = UIColor(red: 0.906, green: 0.055, blue: 0.282, alpha: 1)
        self.nextButton.tintColor = UIColor.whiteColor()
        nextButton.setTitle("NEXT", forState: UIControlState.Normal)
        
        self.skillSetTextView.font = UIFont(name: examplesFont, size:18)
        self.skillSetTextView.tintColor = UIColor.lightGrayColor()
        self.skillSetTextView.layer.borderColor = UIColor(red: 209/255, green: 212/255, blue: 211/255, alpha: 1).CGColor
        self.skillSetTextView.textColor = UIColor(red: 114/255, green: 104/255, blue: 91/255, alpha: 1)
        self.skillSetTextView.layer.borderWidth = 1
        self.skillSetTextView.layer.cornerRadius = 0.5
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        
        if segue.identifier == "SkillsToAddInfo" {
            if let destinationVC = segue.destinationViewController as? UINavigationController {
                println("Entered ExpToSkills")
                var addInfoVC  = destinationVC.viewControllers[0] as? PostAddInfoViewController
                addInfoVC?.postObjectAddInfo = self.postObjectSkills
                addInfoVC?.delegate = self
            }
        }
    }
    
    func PostAddInfoViewControllerDidBack(controller: PostAddInfoViewController) {
        println("PostFinalController back delegate function")
        self.dismissViewControllerAnimated(true, completion:nil)
        //self.tabBarController?.selectedIndex = 0
    }
    
    func textViewShouldReturn (textView: UITextView) -> Bool {
        textView.resignFirstResponder()
        return true
    }
    
    func textViewDidBeginEditing (textView: UITextView) {
        if textView.textColor == UIColor.lightGrayColor() {
            textView.text = nil
            textView.textColor = UIColor.blackColor()
        }
        println("Entered textFieldDidBeginEditing");
        self.currentResponder = textView;
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Please enter a brief description"
            textView.textColor = UIColor.lightGrayColor()
        }
        else {
            self.postObjectQ3.postSkills = textView.text;
            println("postSkills \(self.postObjectQ3.postSkills)");
        }
    }
    
    func tapDetector() {
        self.currentResponder.resignFirstResponder()
    }

    
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
