//
//  PostAddInfoViewController.swift
//  InterviewAssist
//
//  Created by Ranjani  Manchikanti on 5/2/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//

import UIKit

protocol PostAddInfoViewControllerDelegate
{
    func PostAddInfoViewControllerDidBack(controller : PostAddInfoViewController)
}

class PostAddInfoViewController: UIViewController, UITextViewDelegate, PostFinalViewControllerDelegate {
    
    var delegate:PostAddInfoViewControllerDelegate?
    
    var postObjectAddInfo = PostObject()

    @IBOutlet weak var additionalInfoText: UITextView!
    @IBOutlet weak var additionalInfoLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBAction func cancelButtton(sender: AnyObject) {
        println("PostSkillsViewController Cancel Button")
        //Create the AlertController
        let actionSheetController: UIAlertController = UIAlertController(title: "Alert", message: "Are you sure you want to cancel this post?", preferredStyle: .Alert)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
            //Do some stuff
        }
        actionSheetController.addAction(cancelAction)
        //Create and an option action
        let oKAction: UIAlertAction = UIAlertAction(title: "OK", style: .Default) { action -> Void in
            self.performSegueWithIdentifier("AddlInfoCancel", sender: self)
            //self.tabBarController?.selectedIndex = 2
        }
        actionSheetController.addAction(oKAction)
        //        //Add a text field
        //        actionSheetController.addTextFieldWithConfigurationHandler { textField -> Void in
        //            //TextField configuration
        //            textField.textColor = UIColor.blueColor()
        //        }
        //Present the AlertController
        self.presentViewController(actionSheetController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        // navigationItem.title = &quot;One&quot;
        navigationItem.title = "I.A"
        
        var font = UIFont(name: "AvenirNext-DemiBold", size: 20)
        var barTintColor = UIColor(red: 209/255, green: 212/255, blue: 211/255, alpha: 1)
        var tintColor = UIColor(red: 0.906, green: 0.055, blue: 0.282, alpha: 1)
        navigationController?.navigationBar.barTintColor = barTintColor
        navigationController?.navigationBar.tintColor = tintColor
    }
    
    
    @IBAction func backButton(sender: AnyObject) {
        println("PostAddInfoViewController Back Button")
        self.delegate?.PostAddInfoViewControllerDidBack(self)
    }
    @IBOutlet weak var textView1: UITextView!
    var currentResponder:AnyObject = "Nil"
    
    var postObjectQ2 = PostObject()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.additionalInfoText.delegate = self
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("tapDetector"))
        self.view.addGestureRecognizer(gestureRecognizer)
       // self.textView1.text = "Please enter a brief description"
        self.additionalInfoText.textColor = UIColor.lightGrayColor()

        self.navigationController?.navigationBar.tintColor = UIColor(red: 0.906, green: 0.055, blue: 0.282, alpha: 1)
        
        var addtlInfoFont1 = "AvenirNext-Regular"
        var addtlInfoTextViewFont = "AvenirNext-UltraLightItalic"
        
        self.additionalInfoLabel.font = UIFont(name: addtlInfoFont1, size:20)
        self.additionalInfoLabel.text = "Additional Information"
        
        self.nextButton.backgroundColor=UIColor(red: 0.906, green: 0.055, blue: 0.282, alpha: 1)
        self.nextButton.tintColor = UIColor.whiteColor()
        nextButton.setTitle("NEXT", forState: UIControlState.Normal)
        
        self.additionalInfoText.font = UIFont(name: addtlInfoTextViewFont, size:18)
        self.additionalInfoText.tintColor = UIColor.lightGrayColor()
        self.additionalInfoText.layer.borderColor = UIColor(red: 209/255, green: 212/255, blue: 211/255, alpha: 1).CGColor
        self.additionalInfoText.textColor = UIColor(red: 114/255, green: 104/255, blue: 91/255, alpha: 1)
        self.additionalInfoText.layer.borderWidth = 1
        self.additionalInfoText.layer.cornerRadius = 0.5
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        if segue.identifier == "AddInfoToFinal" {
            if let destinationVC = segue.destinationViewController as? UINavigationController {
                println("Entered ExpToSkills")
                var finalVC  = destinationVC.viewControllers[0] as? PostFinalViewController
                finalVC?.postObjectFinal = self.postObjectAddInfo
                finalVC?.delegate = self
            }
        }
    }
    
    func PostFinalViewControllerDidBack(controller: PostFinalViewController) {
        println("PostFinalViewController back delegate function")
        self.dismissViewControllerAnimated(true, completion:nil)
        //self.tabBarController?.selectedIndex = 0
    }
    
    func textViewShouldReturn (textView: UITextView) -> Bool {
        textView.resignFirstResponder()
        return true
    }
    
    func textViewDidBeginEditing (textView: UITextView) {
        if textView.textColor == UIColor.lightGrayColor() {
            textView.text = nil
            textView.textColor = UIColor.blackColor()
        }
        println("Entered textFieldDidBeginEditing");
        self.currentResponder = textView;
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Please enter a brief description"
            textView.textColor = UIColor.lightGrayColor()
        }
        else {
            self.postObjectAddInfo.postAddInfo = textView.text;
            println("postDexcription \(self.postObjectAddInfo.postAddInfo)");
        }
    }
    
    func tapDetector() {
        self.currentResponder.resignFirstResponder()
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
