//
//  AddEditSkillTableViewCell.swift
//  IA
//
//  Created by Rhiya Manchikanti on 6/27/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//
class AddEditSkillTableViewCell: UITableViewCell {

    @IBOutlet weak var skillName: UILabel!
    
    @IBOutlet weak var newSkillName: UITextField!
}
