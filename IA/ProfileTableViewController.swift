//
//  ProfileTableViewController.swift
//  IA
//
//  Created by Rhiya Manchikanti on 6/22/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//



class ProfileTableViewController: UITableViewController {
    
    var searchResults = [PFObject]()
    var profileObject = Profile()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.profileObject = Profile()
        var object:PFObject
        var query:PFQuery = PFQuery(className: "UserProfile")
        var loggedinUserID = PFUser.currentUser()?.objectId
        query.whereKey("userIDptr", matchesRegex:loggedinUserID!)
        query.findObjectsInBackgroundWithBlock{ (objects,error) -> Void in
            if error == nil {
                
                
                if let buddyObjects = objects as? [PFObject] {
                    for buddy in buddyObjects {
                        
                        
                        
                        var firstName:AnyObject!  = buddy.objectForKey("firstName");
                        var lastName:AnyObject! = buddy.objectForKey("lastName");
                        var email:AnyObject! = buddy.objectForKey("emailAddress");
                        var phone:AnyObject! = buddy.objectForKey("phone");
                        var isCoach:AnyObject! = buddy.objectForKey("isCoach")
                        var mockIntExp:AnyObject! = buddy.objectForKey("mockInterviewExperience")
                        var hourlyRate:AnyObject! = buddy.objectForKey("hourlyRate")
                        
                        if (firstName != nil) && !(firstName.isEqual(NSNull())){
                            self.profileObject.firstName = firstName as! String
                        }
                        
                        if (lastName != nil) && !(lastName.isEqual(NSNull())){
                            self.profileObject.lastname = lastName as! String
                        }
                        if (email != nil) && !(email.isEqual(NSNull())){
                            self.profileObject.email = email as! String
                        }
                        if (phone != nil) && !(phone.isEqual(NSNull())){
                            self.profileObject.phone = phone as! String
                        }
                        if (isCoach != nil) && !(isCoach.isEqual(NSNull())){
                            self.profileObject.isCoach = isCoach as! Bool
                        }
                        if (mockIntExp != nil) && !(mockIntExp.isEqual(NSNull())){
                            self.profileObject.coachInterviewExperience = mockIntExp as! String
                        }
                        if (hourlyRate != nil) && !(hourlyRate.isEqual(NSNull())){
                            self.profileObject.coachHourlyRate = hourlyRate as! String
                        }
                        
                        var skillValue: AnyObject! = buddy.objectForKey("skills");                           if (skillValue != nil) && !(skillValue.isEqual(NSNull())){
                            self.profileObject.skillSet =  (buddy.objectForKey("skills") as? Array)!
                        }
                        
                        var eduValue: AnyObject! = buddy.objectForKey("education");                          if (eduValue != nil) && !(eduValue.isEqual(NSNull())){
                            self.profileObject.educationList =  (buddy.objectForKey("education") as? Array)!
                        }
                        
                        var expValue: AnyObject! = buddy.objectForKey("experience");                           if (expValue != nil) && !(expValue.isEqual(NSNull())){
                            self.profileObject.experienceList =  (buddy.objectForKey("experience") as? Array)!
                        }

                       
                        
                        
                        
                      
                       
                        
                       
                        
                    }
                }
            }
            
       }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 5
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        
        
        return 1
        
        
        
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("profiletableviewcell", forIndexPath: indexPath) as! ProfileTableViewCell
//        if(indexPath.section==1){
//                       cell.outputLabel.text = self.skills
//        }else if(indexPath.section==2){
//            cell.outputLabel.text = self.education
//        }else if(indexPath.section==3){
//            cell.outputLabel.text = self.experience
//        }
        
        return cell
        
        
        
    }
    
    
    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    
    
    
    
    
    
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
        
    }
    
    
    
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
       
        
        
        if segue.identifier == "editProfileSegue" {
            if let destinationVC = segue.destinationViewController as? UINavigationController {
                println("Entered ExpToSkills")
                var editProfileVC  = destinationVC.viewControllers[0] as? EditProfileTableViewController
                editProfileVC?.editProfileObject = self.profileObject
                
               
            }
        }
    }
    
        
    
    
    
    override func tableView(tableView: UITableView,
        titleForHeaderInSection section: Int) -> String?{
            //println("Entered here")
            if (section == 0){
                return "Profile"
            }
            else if (section == 1){
                return "Skills"
            }
            else if (section == 2){
                return "Education"
            }
            else if (section == 3){
                return "Projects"
            }
            else if (section == 4){
                return "More"
            }
            else{
                return ""
            }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Get the row data for the selected row
        
    }
    
    

}
