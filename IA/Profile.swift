//
//  Profile.swift
//  IA
//
//  Created by Rhiya Manchikanti on 6/28/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//

import Foundation


struct Education{
    var nameOfInstitution = String();
    var degree = String();
    var startDate = String();
    var endDate = String();
    init(){
        nameOfInstitution="Institution Name"
        degree="Degree"
        startDate="Start Year"
        endDate="End Year"
    }
}

struct Experience{
    var companyName = String();
    var title = String();
    var startDate = String();
    var endDate = String();
    init(){
        companyName="Company Name"
        title="Title"
        startDate="Start Date"
        endDate="End Date"
    }
}

class Profile{
    
    var firstName:String = "First Name"
    var lastname:String = "Last Name"
    var email:String = "Email"
    var phone:String = "Phone";
    var isCoach = Bool(false)
    var coachHourlyRate = "Hourly Rate"
    var coachInterviewExperience = "Mock INterview Experience"
    var skillSet = [String]()
    
    var educationList:[Education] = []
    var experienceList:[Experience] = []
    
    
    
}
