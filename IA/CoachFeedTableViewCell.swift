//
//  CoachFeedTableViewCell.swift
//  IA
//
//  Created by Ranjani  Manchikanti on 6/21/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//

import UIKit

class CoachFeedTableViewCell: PFTableViewCell  {

    @IBOutlet weak var coachName: UILabel!
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var currentJob: UILabel!
    
    @IBOutlet weak var skills: UILabel!
    
    @IBOutlet weak var ratingImage: UIImageView!
    
    @IBOutlet weak var ratingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
