//
//  BuddyDetailTableViewController.swift
//  InterviewAssist
//
//  Created by Ranjani  Manchikanti on 5/10/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//

import UIKit

class BuddyDetailTableViewController: UITableViewController {
    
    var detailObject = PostObject()
    
    override func viewWillAppear(animated: Bool) {
        // navigationItem.title = &quot;One&quot;
        navigationItem.title = "I.A"
        
        var font = UIFont(name: "AvenirNext-DemiBold", size: 20)
        var barTintColor = UIColor(red: 246/255, green: 250/255, blue: 249/255, alpha: 0.5)
        var tintColor = UIColor(red: 0.906, green: 0.055, blue: 0.282, alpha: 1)
        navigationController?.navigationBar.barTintColor = barTintColor
        navigationController?.navigationBar.tintColor = tintColor
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.tableView.separatorStyle = .None
        self.tableView.tableFooterView = UIView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return 3
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
            return 1
    }
    
    override func tableView( tableView : UITableView,  titleForHeaderInSection section: Int)->String {
        println("Entered here")
        switch(section) {
        case 0:
            return "Desired requirements"
        case 1:
            return "Additional information"
        case 2:
            return "case-3"
        default :
            return "abc"
            
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("BuddyDetailTableViewCell", forIndexPath: indexPath) as! BuddyDetailTableViewCell
        if (indexPath.section == 0) {
            let cell = tableView.dequeueReusableCellWithIdentifier("BuddyDetailTableViewCell", forIndexPath: indexPath) as! BuddyDetailTableViewCell
            cell.skillSet.text = self.detailObject.postSkills
            cell.experienceLevelLabel.text = self.detailObject.postExp
        }
        else if (indexPath.section == 1) {
            let cell = tableView.dequeueReusableCellWithIdentifier("BuddyDetailInfoTableViewCell", forIndexPath: indexPath) as! BuddyDetailInfoTableViewCell
            cell.additionaInfoText.text = self.detailObject.postAddInfo
        }
        else if (indexPath.section == 2) {
            let cell = tableView.dequeueReusableCellWithIdentifier("BuddyDetailContactTableViewCell", forIndexPath: indexPath) as! BuddyDetailContactTableViewCell
            
            cell.profileBtn.addTarget(self, action:"ProfileButton:", forControlEvents:UIControlEvents.TouchUpInside)
            cell.chatBtn.addTarget(self, action:"ContactButton:", forControlEvents:UIControlEvents.TouchUpInside)
        }
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    
        switch(section) {
        case 0:
            return 50
        case 1:
            return 50
        case 2:
            return 50
        default:
            return 0
            
        }
    }
    
    override func tableView(tableView: UITableView!, heightForRowAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
      
        return 135
            
        
        
    }
    
    func ProfileButton(sender:UIButton)
    {
        println("hello")
    }
    
    func ContactButton(sender:UIButton)
    {
        println("hello")
    }

//    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell,
//        forRowAtIndexPath indexPath: NSIndexPath)
//    {
//        // Remove separator inset
//        if cell.respondsToSelector("setSeparatorInset:") {
//            cell.separatorInset = UIEdgeInsetsZero
//        }
//        
//        // Prevent the cell from inheriting the Table View's margin settings
//        if cell.respondsToSelector("setPreservesSuperviewLayoutMargins:") {
//            cell.preservesSuperviewLayoutMargins = false
//        }
//        
//        // Explictly set your cell's layout margins
//        if cell.respondsToSelector("setLayoutMargins:") {
//            cell.layoutMargins = UIEdgeInsetsZero
//        }
//    }

    
}
