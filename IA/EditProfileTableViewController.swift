//
//  EditProfileTableViewController.swift
//  IA
//
//  Created by Rhiya Manchikanti on 6/22/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//



class EditProfileTableViewController: UITableViewController,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,UITextFieldDelegate{
    
    
    var numberOfSkills = 1
    var numberOfEducations = 1
    var numberOfExperiences = 1
    var profileObject = Profile()
    var pre_existing_skills = [String]()
    //var pre_existing_ed
    var editProfileObject:Profile!
    
    
    
   
    override func viewDidLoad() {
        
        println(self.profileObject)
        self.tableView.setEditing(true, animated: true)
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("tapDetector"))
        gestureRecognizer.cancelsTouchesInView = false;
        self.view.addGestureRecognizer(gestureRecognizer)
        self.editProfileObject = Profile()
         var edu = Education.init()
        var exp = Experience.init()
        loadExistingProfileData()
        
        
        //put atleast one member in array as you donot insert rows in screen on the first load
        //i.r the edit screen is showed with atleast one row of empty/filled data
       
        editProfileObject.educationList.append(edu)
        
        
        editProfileObject.experienceList.append(exp)
        
        editProfileObject.skillSet.append("")
        
    }
    
    func loadExistingProfileData() -> Void{
        
        
    }
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if(editProfileObject?.isCoach==true){
            return 5
        }else{
            return 4
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section==1){
            return numberOfSkills
        }
        if(section==2){
            return numberOfEducations
        }
        if(section==3){
            return numberOfExperiences
        }
        return 1
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        if(indexPath.section==1){
            let cell = tableView.dequeueReusableCellWithIdentifier("addEditSkillCell", forIndexPath: indexPath) as! AddEditSkillTableViewCell
            cell.setEditing(true, animated: true)
            cell.skillName.text = "Add skill"
            cell.newSkillName.delegate = self
            cell.newSkillName.tag = 700 + (indexPath.row)
            return cell;
            
        }
        else if(indexPath.section==0){
            let basicProfileCell = tableView.dequeueReusableCellWithIdentifier("basicprofilecellidentifier", forIndexPath: indexPath) as! BasicProfileTableViewCell
            
            basicProfileCell.coachAnswer_Outlet.addTarget(self, action:"CoachAnswer:", forControlEvents:UIControlEvents.TouchUpInside)
            
            
            basicProfileCell.firstNameTextField.delegate=self
            basicProfileCell.lastNameTextField.delegate=self
            basicProfileCell.emailTextField.delegate=self
            basicProfileCell.phoneTextField.delegate=self
            
            
            basicProfileCell.firstNameTextField.placeholder = editProfileObject.firstName
            basicProfileCell.lastNameTextField.placeholder = editProfileObject.lastname
            basicProfileCell.emailTextField.placeholder = editProfileObject.email
            basicProfileCell.phoneTextField.placeholder=editProfileObject.phone
            
            basicProfileCell.firstNameTextField.tag = 100
            basicProfileCell.lastNameTextField.tag = 101
            basicProfileCell.emailTextField.tag = 102
            basicProfileCell.phoneTextField.tag = 103
            
            
            return basicProfileCell;
        }
        else if(indexPath.section==2){
            
            let educationCell = tableView.dequeueReusableCellWithIdentifier("educationcellidentifier", forIndexPath: indexPath) as! EducationTableViewCell
            
            educationCell.setEditing(true, animated: true)
            educationCell.institutionName.delegate = self
            educationCell.degreeTextField.delegate=self
            educationCell.startDateTextField.delegate=self
            educationCell.endDateTextField.delegate=self
            
            educationCell.institutionName.placeholder = editProfileObject.educationList[indexPath.row].nameOfInstitution
            educationCell.degreeTextField.placeholder=editProfileObject.educationList[indexPath.row].degree

            educationCell.startDateTextField.placeholder=editProfileObject.educationList[indexPath.row].startDate

            educationCell.endDateTextField.placeholder=editProfileObject.educationList[indexPath.row].endDate

            
            educationCell.institutionName.tag = 400 + (indexPath.row*4);
            educationCell.degreeTextField.tag = 400+(indexPath.row)+1
            educationCell.startDateTextField.tag = 400 + (indexPath.row*4)+2
            educationCell.endDateTextField.tag = 400 + (indexPath.row*4)+3
            
            return educationCell;
            
        }else if (indexPath.section==3){
            let experienceCell = tableView.dequeueReusableCellWithIdentifier("experienceidentifier", forIndexPath: indexPath) as! ExperienceTableViewCell
            experienceCell.setEditing(true, animated: true)
            experienceCell.companyName.delegate = self
            experienceCell.title.delegate = self
            experienceCell.startDate.delegate = self
            experienceCell.endDate.delegate = self
            
            experienceCell.companyName.placeholder = editProfileObject.experienceList[indexPath.row].companyName

            experienceCell.title.placeholder = editProfileObject.experienceList[indexPath.row].title

            experienceCell.startDate.placeholder = editProfileObject.experienceList[indexPath.row].startDate

            experienceCell.endDate.placeholder = editProfileObject.experienceList[indexPath.row].endDate

            
            experienceCell.companyName.tag = 900 + (indexPath.row*4);
            experienceCell.title.tag = 900 + (indexPath.row*4) + 1;
            experienceCell.startDate.tag = 900 + (indexPath.row*4) + 2;
            experienceCell.endDate.tag = 900 + (indexPath.row*4) + 3;
            
            return experienceCell
            
            
            
        }else if (indexPath.section==4){
            let coachCell = tableView.dequeueReusableCellWithIdentifier("coachCellIdentifier", forIndexPath: indexPath) as! CoachTableViewCell
            
            coachCell.hourlyRate.placeholder = "Enter hourly rate"
            
            coachCell.hourlyRate.delegate = self
            coachCell.mockInterviewExperience.delegate = self
            coachCell.hourlyRate.tag = 1100
            coachCell.mockInterviewExperience.tag = 1111
            
            return coachCell
        }
            
        else{
            
            let cell = tableView.dequeueReusableCellWithIdentifier("addEditSkillCell", forIndexPath: indexPath) as! AddEditSkillTableViewCell
            return cell
        }
        
        
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if (indexPath.section == 0) {
            return 260;
        }
        else if (indexPath.section == 1) {
            return 150;
        } else if (indexPath.section == 2) {
            return 250;
        }else if (indexPath.section == 3) {
            return 250;
        }else{
            return 200;
        }
        
    }
    
    func CoachAnswer(sender:UISwitch){
        if(sender.on){
            editProfileObject.isCoach = true
            self.tableView.reloadData()
            println("coach is on")
        }
    }
    
    
    func textViewDidEndEditing(textView: UITextView){
        if(textView.tag==1111){
            editProfileObject.coachInterviewExperience = textView.text
            println(editProfileObject.coachInterviewExperience)
        }
        
    }
    func textFieldDidEndEditing(textField: UITextField){
        if ( textField.tag>=700 && textField.tag<800){
            var index = textField.tag-700
            editProfileObject.skillSet[index] = textField.text
            println(editProfileObject.skillSet)
        }
        if(textField.tag==100){
            editProfileObject.firstName = textField.text
            println(editProfileObject.firstName)
        }
        if(textField.tag==101){
            editProfileObject.lastname = textField.text
            println(editProfileObject.lastname)
        }
        if(textField.tag==102){
            editProfileObject.email = textField.text
            println(editProfileObject.email)
        }
        if(textField.tag==103){
            editProfileObject.phone = textField.text
            println(editProfileObject.phone)
        }
        
        if(textField.tag >= 400 && textField.tag < 500){
            
            var arrIndex = ((textField.tag-400 )/4)
            
            if(textField.tag % 4 == 0){
                editProfileObject.educationList[arrIndex].nameOfInstitution = textField.text
            }
            
            if(textField.tag % 4 == 1){
                editProfileObject.educationList[arrIndex].degree = textField.text
            }
            if(textField.tag % 4 == 2){
                editProfileObject.educationList[arrIndex].startDate = textField.text
                
            }
            if(textField.tag % 4 == 3){
                
                editProfileObject.educationList[arrIndex].endDate = textField.text
            }
            
            
        }
        if(textField.tag >= 900 && textField.tag < 1000){
            
            var arrIndex = ((textField.tag-900 )/4)
            
            if(textField.tag % 4 == 0){
                editProfileObject.experienceList[arrIndex].companyName = textField.text
            }
            
            if(textField.tag % 4 == 1){
                editProfileObject.experienceList[arrIndex].title = textField.text
            }
            if(textField.tag % 4 == 2){
                editProfileObject.experienceList[arrIndex].startDate = textField.text
                
            }
            if(textField.tag % 4 == 3){
                
                editProfileObject.experienceList[arrIndex].endDate = textField.text
            }
            
            
        }
        
        if(textField.tag==1100){
            editProfileObject.coachHourlyRate = textField.text
            println(editProfileObject.coachHourlyRate)
        }
        

        
        
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
    }
    
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
        
    }
    
    
    
    // Override to support conditional rearranging of the table view.
    
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
    }
    
    
    
    override func tableView(tableView: UITableView,
        titleForHeaderInSection section: Int) -> String?{
            //println("Entered here")
            if (section == 0){
                return "Basic Profile"
            }
            else if (section == 1){
                return "Skills"
            }
            else if (section == 2){
                return "Education"
            }
            else if (section == 3){
                return "Experience"
            }
            else if (section == 4){
                return "Coach"
            }
            else{
                return ""
            }
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool{
        return true
    }
    
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Get the row data for the selected row
        
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath){
        if(indexPath.section==1){
            if (editingStyle == .Delete) {
                numberOfSkills = numberOfSkills-1
                editProfileObject.skillSet.removeAtIndex(indexPath.row)
                self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                
            }
            else if(editingStyle == .Insert){
                
                numberOfSkills = numberOfSkills+1
                editProfileObject.skillSet.append((" "))
                self.tableView.reloadData()
                
            }
        }else if(indexPath.section==2){
            if (editingStyle == .Delete) {
                numberOfEducations = numberOfEducations-1
                editProfileObject.educationList.removeAtIndex(indexPath.row)
                self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                
            }
            else if(editingStyle == .Insert){
                
                numberOfEducations = numberOfEducations+1
                var edu = Education.init()
                editProfileObject.educationList.append(edu)
                self.tableView.reloadData()
                
            }
        }
        else if(indexPath.section==3){
            if (editingStyle == .Delete) {
                numberOfExperiences = numberOfExperiences-1
                editProfileObject.educationList.removeAtIndex(indexPath.row)
                self.tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
                
            }
            else if(editingStyle == .Insert){
                
                numberOfExperiences = numberOfExperiences+1
                var exp = Experience.init()
                editProfileObject.experienceList.append(exp)
                self.tableView.reloadData()
                
            }
        }
        
        
    }
    override func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        
        if (indexPath.section == 1) {
            if ((indexPath.row == numberOfSkills-1)) {
                return .Insert;
            } else {
                return .Delete;
            }
        }
        if (indexPath.section == 2) {
            if ((indexPath.row == numberOfEducations-1)) {
                return .Insert;
            } else {
                return .Delete;
            }
        }
        if (indexPath.section == 3) {
            if ((indexPath.row == numberOfExperiences-1)) {
                return .Insert;
            } else {
                return .Delete;
            }
        }
        else {
            return .None;
        }
        
    }
    
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if text == "\n"
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /**
    * Called when the user click on the view (outside the UITextField).
    */
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
    
    
    func tapDetector() {
        self.view.endEditing(true)
    }
    
    
    @IBAction func saveEditProfile(sender: AnyObject) {
        println(editProfileObject)
        
         var currentUserID = PFUser.currentUser()?.objectId
         var userProfileClass = PFObject(className: "UserProfile")
         var query = PFQuery(className:"UserProfile")
         query.whereKey("userIDptr", matchesRegex:currentUserID!)
        
        
        
//        query.findObjectsInBackgroundWithBlock{ (objects,error) -> Void in
//            if error == nil {
//                
//                
//                if let userObjects = objects as? [PFObject] {
//                    for userObj in userObjects {
//                        
//                        var skillsArr = [String]()
//                        var experienceArr = [String]()
//                        var educationArr = [String]()
//                        
//                        skillsArr = (userObj.objectForKey("skills") as? Array)!
//                        educationArr = (userObj.objectForKey("education") as? Array)!
//                        experienceArr = (userObj.objectForKey("projects") as? Array)!
//                        
//                        
//                        
//                        let delimiter = ","
//                        
//                        self.skills = join(delimiter, skillsArr)
//                        self.education = join(delimiter,educationArr)
//                        self.projects = join(delimiter,projectsArr)
//                        
//                    }
//                }
//            }
//
//            
//            
//            
//        }
        
    }
    
}
