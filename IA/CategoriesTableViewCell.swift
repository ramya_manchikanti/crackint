//
//  CategoriesTableViewCell.swift
//  InterviewAssist
//
//  Created by Ranjani  Manchikanti on 5/9/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//

import UIKit

class CategoriesTableViewCell: UITableViewCell {

    @IBOutlet weak var categoriesImage: UIImageView!

    @IBOutlet weak var postsCountLabel: UILabel!
  //  @IBOutlet weak var categoriesLabelImage: UIImageView!
    @IBOutlet weak var categoriesTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        self.categoriesImage.contentMode = UIViewContentMode.ScaleAspectFit

        
        var cellTitleFont = "AvenirNext-Bold"
        categoriesTitleLabel.font = UIFont(name: cellTitleFont, size:17.0)
        postsCountLabel.font = UIFont(name: "AvenirNext-Italic", size:12.0)
        postsCountLabel.textColor = UIColor.whiteColor()
        
        
            
        //PINKY: check if blur needs to be in awakefromnib or init method
        // create effect
//        categoriesLabelImage.backgroundColor = UIColor.clearColor()
//        var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light)) as UIVisualEffectView
//        visualEffectView.frame = categoriesLabelImage.bounds
//        categoriesLabelImage.addSubview(visualEffectView)

        
        // self.cuisineImageView.layer.cornerRadius = 9.0f;
   //     self.categoriesImage.layer.masksToBounds = true

        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
