//
//  PostTitleViewController.swift
//  InterviewAssist
//
//  Created by Rhiya Manchikanti on 4/26/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//

import UIKit

protocol PostTitleViewControllerDelegate
{
    func PostTitleViewControllerDidBack(controller : PostTitleViewController)
}

class PostTitleViewController: UIViewController, UITextFieldDelegate, PostExpTableViewControllerDelegate {
    
    var delegate:PostTitleViewControllerDelegate?

    //outlets
    
   
    
    @IBOutlet weak var postTitle: UILabel!


    @IBOutlet weak var examplesLabel: UITextField!
    @IBOutlet weak var nextButton: UIButton!
    
    
    @IBAction func backButton(sender: AnyObject) {
        println("PostSkillsViewController Back Button")
        self.delegate?.PostTitleViewControllerDidBack(self)
    }
    
    @IBAction func cancelButton(sender: AnyObject) {
        println("PostSkillsViewController Cancel Button")
        //Create the AlertController
        let actionSheetController: UIAlertController = UIAlertController(title: "Alert", message: "Are you sure you want to cancel this post?", preferredStyle: .Alert)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
            //Do some stuff
        }
        actionSheetController.addAction(cancelAction)
        //Create and an option action
        let oKAction: UIAlertAction = UIAlertAction(title: "OK", style: .Default) { action -> Void in
            self.performSegueWithIdentifier("TitleCancel", sender: self)
            //self.tabBarController?.selectedIndex = 2
        }
        actionSheetController.addAction(oKAction)
        //        //Add a text field
        //        actionSheetController.addTextFieldWithConfigurationHandler { textField -> Void in
        //            //TextField configuration
        //            textField.textColor = UIColor.blueColor()
        //        }
        //Present the AlertController
        self.presentViewController(actionSheetController, animated: true, completion: nil)
    }
    var currentResponder:AnyObject = "Nil"
    var postObjectTitle = PostObject()
  
    //var delegate:LoginViewControllerDelegate?
    //var delegate1:PostQuestion2ViewControllerDelegate?
    
    override func viewWillAppear(animated: Bool) {
        // navigationItem.title = &quot;One&quot;
        navigationItem.title = "I.A"
        navigationController?.navigationBar.barTintColor = UIColor(red: 0.906, green: 0.055, blue: 0.282, alpha: 1)
        navigationController?.navigationBar.tintColor = UIColor(red: 0.906, green: 0.055, blue: 0.282, alpha: 1)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        
      //Pinky: commented - to check:  self.textField1.delegate = self
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("tapDetector"))
        self.view.addGestureRecognizer(gestureRecognizer)
        // Do any additional setup after loading the view.
        
        self.navigationController?.navigationBar.tintColor = UIColor(red: 0.906, green: 0.055, blue: 0.282, alpha: 1)
        
        var postTitleFont1 = "AvenirNext-Regular"
        var examplesFont = "AvenirNext-UltraLightItalic"
        
        self.postTitle.font = UIFont(name: postTitleFont1, size:20)
        self.postTitle.text = "Post Title"
        
        self.nextButton.backgroundColor=UIColor(red: 0.906, green: 0.055, blue: 0.282, alpha: 1)
        self.nextButton.tintColor = UIColor.whiteColor()
        nextButton.setTitle("NEXT", forState: UIControlState.Normal)
        
        self.examplesLabel.font = UIFont(name: examplesFont, size:13)
        self.examplesLabel.tintColor = UIColor.lightGrayColor()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        

        if segue.identifier == "TitleToExp" {
                if let destinationVC = segue.destinationViewController as? UINavigationController {
                    println("Entered Question2Segue")
                    var expVC  = destinationVC.viewControllers[0] as? PostExpTableViewController
                    expVC?.postObjectExp = self.postObjectTitle
                    expVC?.delegate = self
                }
        }
    }
    
    
    func PostExpTableViewControllerDidBack(controller: PostExpTableViewController)
    {
        println("PostExpTableViewController back delegate function")
        self.dismissViewControllerAnimated(true, completion:nil)
        //self.tabBarController?.selectedIndex = 0
    }
    
    func textFieldShouldReturn (textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing (textField: UITextField) {
        println("Entered textFieldDidBeginEditing");
        self.currentResponder = textField;
    }
    
    func textFieldDidEndEditing (textField: UITextField) {
        self.postObjectTitle.postTitle = textField.text;
        println("postTitle \(self.postObjectTitle.postTitle)");
    }
    
    func tapDetector() {
        self.currentResponder.resignFirstResponder()
    }
    
    
}
