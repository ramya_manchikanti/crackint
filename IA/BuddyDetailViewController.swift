//
//  BuddyDetailViewController.swift
//  InterviewAssist
//
//  Created by Ranjani  Manchikanti on 5/16/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//

import UIKit

class BuddyDetailViewController: UIViewController {

    @IBOutlet weak var AddtlInformationLabel: UILabel!
    @IBOutlet weak var postTitleLabel: UILabel!
    
    @IBOutlet weak var addtnlInfoText: UITextView!
    @IBOutlet weak var skillSetTextView: UITextView!
    @IBOutlet weak var areaOfExpertiseLabel: UILabel!
    @IBOutlet weak var requiredExperienceLavelLabel: UILabel!
    @IBOutlet weak var experienceLevelLabel: UILabel!
  
    @IBOutlet weak var userProfileBtn: UIButton!
    
    @IBOutlet weak var chatButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        var postTitleFont = "AvenirNext-DemiBold"
        var labelFont = "AvenirNext-Medium"
        var resultFont = "AvenirNext-Regular"
        
        self.postTitleLabel.font = UIFont(name: postTitleFont, size:16.0)
        self.postTitleLabel.text = "Hadoop Engineer at Google"
        
        self.requiredExperienceLavelLabel.font = UIFont(name: labelFont, size:15.0)
        self.requiredExperienceLavelLabel.textColor = UIColor.grayColor()
        self.requiredExperienceLavelLabel.text = "Desired experience level:"
        
        self.experienceLevelLabel.font = UIFont(name: resultFont, size:14.0)
        self.experienceLevelLabel.textColor = UIColor.grayColor()
        self.experienceLevelLabel.text = "1-5 yrs"
        
        self.areaOfExpertiseLabel.font = UIFont(name: labelFont, size:15.0)
        self.areaOfExpertiseLabel.textColor = UIColor.grayColor()
        self.areaOfExpertiseLabel.text = "Desired skill set:"
        
        self.skillSetTextView.font = UIFont(name: resultFont, size:14.0)
        self.skillSetTextView.textColor = UIColor.grayColor()
        self.skillSetTextView.text = "VLSI, Analog devices, MIPS, Chip Designing"
        
        self.AddtlInformationLabel.font = UIFont(name: labelFont, size:15.0)
        self.AddtlInformationLabel.textColor = UIColor.grayColor()
        self.AddtlInformationLabel.text = "Additional Info:"
        
        self.addtnlInfoText.font = UIFont(name: resultFont, size:14.0)
        self.addtnlInfoText.textColor = UIColor.grayColor()
        self.addtnlInfoText.text = "VLSI, Analog devices, MIPS, Chip Designing"

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
