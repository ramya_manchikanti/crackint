//
//  BuddyDetailContactTableViewCell.swift
//  InterviewAssist
//
//  Created by Ranjani  Manchikanti on 5/17/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//

import UIKit

class BuddyDetailContactTableViewCell: UITableViewCell {

    @IBOutlet weak var chatBtn: UIButton!
    @IBOutlet weak var profileBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
