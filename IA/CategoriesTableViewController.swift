//
//  CategoriesTableViewController.swift
//  InterviewAssist
//
//  Created by Ranjani  Manchikanti on 5/9/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//

import UIKit



class CategoriesTableViewController: UITableViewController {
    
    var subCategory = ["Banking","Legal","Software", "Hardware", "Manufacturing"]
    //Dicitionary for finding no of posts in each subCategory
    var subCategory_count = ["Banking":0,
        "Legal":0,
        "Software":0,
        "Hardware":0,
        "Manufacturing":0,
    ]
    var mainCategory:NSString = ""
    var selectedSection:Int = 0

    override func viewWillAppear(animated: Bool) {
        // navigationItem.title = &quot;One&quot;
        navigationItem.title = "I.A"
        
        var font = UIFont(name: "AvenirNext-DemiBold", size: 20)
        var barTintColor = UIColor(red: 246/255, green: 250/255, blue: 249/255, alpha: 0.5)
        var tintColor = UIColor(red: 0.906, green: 0.055, blue: 0.282, alpha: 1)
        navigationController?.navigationBar.barTintColor = barTintColor
        navigationController?.navigationBar.tintColor = tintColor
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.layer.cornerRadius = 3
        self.tableView.reloadData()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        var object:PFObject
        var cat_key:String=""
        var query:PFQuery = PFQuery(className: "InterviewPost")
        query.orderByDescending("createdAt")
        query.findObjectsInBackgroundWithBlock{ (objects,error) -> Void in
            if error == nil {
                for object in objects! {
                    cat_key = (object["Category"] as? String)!
                    var count = self.subCategory_count[cat_key as String]!
                    count = count + 1
                    self.subCategory_count[cat_key as String]! = count
                    println("Sub Category \(cat_key) Count \(count)")
                    //self.array.addObject(object)
                }
                let sortedArray = sorted(self.subCategory_count, {$0.1 > $1.1})
                self.subCategory = sortedArray.map {return $0.0 }
                self.tableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Potentially incomplete method implementation.
        // Return the number of sections.
        return self.subCategory.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // #warning Incomplete method implementation.
        // Return the number of rows in the section.
        //return self.category.count
        return 1
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1.5
    }
    
    override func tableView(tableView: UITableView!, heightForRowAtIndexPath indexPath: NSIndexPath!) -> CGFloat {
        
        return 210
        
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CategoriesTableViewCell", forIndexPath: indexPath) as! CategoriesTableViewCell
        
 
        // Configure the cell...
        //var categoriesLabelFont = "AvenirNext-Medium"
//        var ImageName = self.category[indexPath.row] + ".jpg"
        var cuisine_key:String
        var post_count:Int
        var ImageName = self.subCategory[indexPath.section] + ".jpg"
        cell.categoriesImage?.image = UIImage(named:ImageName)
        
        
        var cellTitleFont = "AvenirNext-Heavy"
        cell.categoriesTitleLabel.font = UIFont(name: cellTitleFont, size:16.0)
        cell.categoriesTitleLabel?.text = self.subCategory[indexPath.section]
        cell.categoriesTitleLabel.textColor = UIColor.whiteColor()
        
        cell.postsCountLabel.font = UIFont(name: "AvenirNext-BoldItalic", size:13.0)
        cell.postsCountLabel.textColor = UIColor.whiteColor()
        cuisine_key = self.subCategory[indexPath.section]
        post_count = self.subCategory_count[cuisine_key]!
        cell.postsCountLabel.text = String(post_count) + " POSTS"
        //Pinky - this wont work in ios7 , check postexptable
        
        cell.backgroundColor = UIColor.clearColor()
        
        
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        selectedSection = indexPath.section
        if self.mainCategory == "Buddy" {
            self.performSegueWithIdentifier("BuddyFeedSegue", sender: self)
        }
        else {
            self.performSegueWithIdentifier("CoachFeedSegue", sender: self)
        }
        //itemId[1] - Item Id
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "BuddyFeedSegue" {
            if let destination = segue.destinationViewController as? BuddyFeedTableViewController {
                //if let categoryNumber = tableView.indexPathForSelectedRow()?.row {
                    destination.category = self.subCategory[selectedSection]
                    println("Destination Category \(self.subCategory[selectedSection]) Category Number \(selectedSection)")
                //}
            }
        }
        else if segue.identifier == "CoachFeedSegue" {
            if let destination = segue.destinationViewController as? CoachFeedTableViewController {
                if let categoryNumber = tableView.indexPathForSelectedRow()?.row {
                    destination.category = self.subCategory[categoryNumber]
                }
            }
        }
    }
    
    /*override func tableView(tableView: (UITableView!), heightForRowAtIndexPath indexPath: (NSIndexPath!)) -> CGFloat {
        
        return 35
        
    }*/
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell,
        forRowAtIndexPath indexPath: NSIndexPath)
    {
        // Remove separator inset
        if cell.respondsToSelector("setSeparatorInset:") {
            cell.separatorInset = UIEdgeInsetsZero
        }
        
        // Prevent the cell from inheriting the Table View's margin settings
        if cell.respondsToSelector("setPreservesSuperviewLayoutMargins:") {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        // Explictly set your cell's layout margins
        if cell.respondsToSelector("setLayoutMargins:") {
            cell.layoutMargins = UIEdgeInsetsZero
        }
    }

}
