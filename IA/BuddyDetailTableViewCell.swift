//
//  BuddyDetailTableViewCell.swift
//  InterviewAssist
//
//  Created by Ranjani  Manchikanti on 5/10/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//

import UIKit

class BuddyDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var skillSet: UITextView!
    @IBOutlet weak var requiredSkillSetLabel: UILabel!
    @IBOutlet weak var experienceLevelLabel: UILabel!
    @IBOutlet weak var requiredExperienceLevelLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        var postTitleFont = "AvenirNext-DemiBold"
        var labelFont = "AvenirNext-Medium"
        var resultFont = "AvenirNext-Regular"
        
        self.requiredExperienceLevelLabel.font = UIFont(name: labelFont, size:15.0)
        self.requiredExperienceLevelLabel.textColor = UIColor.grayColor()
        self.requiredExperienceLevelLabel.text = "Experience level:"
        
        self.experienceLevelLabel.font = UIFont(name: resultFont, size:14.0)
        self.experienceLevelLabel.textColor = UIColor.grayColor()
        self.experienceLevelLabel.text = "1-5 yrs"
        
        self.requiredSkillSetLabel.font = UIFont(name: labelFont, size:15.0)
        self.requiredSkillSetLabel.textColor = UIColor.grayColor()
        self.requiredSkillSetLabel.text = "Skill set:"
        
        self.skillSet.font = UIFont(name: resultFont, size:14.0)
        self.skillSet.textColor = UIColor.grayColor()
        self.skillSet.text = "VLSI, Analog devices, MIPS, Chip Designing"
        
    
    
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
