//
//  BuddyDetailInfoTableViewCell.swift
//  InterviewAssist
//
//  Created by Ranjani  Manchikanti on 5/17/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//

import UIKit

class BuddyDetailInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var additionaInfoText: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        var resultFont = "AvenirNext-Regular"
        self.additionaInfoText.font = UIFont(name: resultFont, size:14.0)
        self.additionaInfoText.textColor = UIColor.grayColor()
        self.additionaInfoText.text = "VLSI, Analog devices, MIPS, Chip Designing"

        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
