//
//  HomeTableViewCell.swift
//  InterviewAssist
//
//  Created by Ranjani  Manchikanti on 4/19/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    
    @IBOutlet weak var cellTitle: UILabel!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var bottomBarImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        var cellTitleFont = "HelveticaNeue-Bold"
        cellTitle.font = UIFont(name: cellTitleFont,size: 20)
        cellTitle.font = cellTitle.font.fontWithSize(20)
        cellTitle.textColor = UIColor.whiteColor()
    //    cellTitle.set
       // backgroundImage.tintColor = UIColor.greenColor()
        
        //PINKY: check if blur needs to be in awakefromnib or init method
        // create effect
        bottomBarImage.backgroundColor = UIColor.clearColor()
        var visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light)) as UIVisualEffectView
        visualEffectView.frame = bottomBarImage.bounds
        bottomBarImage.addSubview(visualEffectView)
        
        
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
  
}
