//
//  PostObject.swift
//  InterviewAssist
//
//  Created by Ranjani  Manchikanti on 5/2/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//

import UIKit

class PostObject: NSObject {

    var postTitle:String=""
    var postAddInfo:String=""
    var postCategory:String=""
    var postSkills:String=""
    var postExp:String=""
    var postCommMode:String=""
}
