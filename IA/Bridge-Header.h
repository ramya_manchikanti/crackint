//
//  Bridge-Header.h
//  IA
//
//  Created by Rhiya Manchikanti on 6/20/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//

#ifndef IA_Bridge_Header_h
#define IA_Bridge_Header_h


#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Parse/Parse.h>
#import <Parse/PFObject+Subclass.h>
#import <ParseUI/ParseUI.h>
#import <ParseFacebookUtilsV4/PFFacebookUtils.h>
#import <Bolts/Bolts.h>

#endif
