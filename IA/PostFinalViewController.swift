//
//  PostFinalViewController.swift
//  InterviewAssist
//
//  Created by Ranjani  Manchikanti on 5/3/15.
//  Copyright (c) 2015 Rhiya Manchikanti. All rights reserved.
//

import UIKit

protocol PostFinalViewControllerDelegate
{
    func PostFinalViewControllerDidBack(controller : PostFinalViewController)
}

class PostFinalViewController: UIViewController {
    
    var delegate:PostFinalViewControllerDelegate?
    var postObjectFinal = PostObject()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var skillsLabel: UILabel!
    

    
    @IBOutlet weak var postFinalTitleText: UITextView!
    
    @IBOutlet weak var postFinalSkillsText: UITextView!
    @IBOutlet weak var postItFinalButton: UIButton!
    @IBOutlet weak var postFinalDescText: UITextView!
    
    
    @IBAction func backButton(sender: AnyObject) {
        println("PostFinalViewController Back Button")
        self.delegate?.PostFinalViewControllerDidBack(self)
    }

    @IBAction func cancelButton(sender: AnyObject) {
        println("PostFinalViewController Cancel Button")
        //Create the AlertController
        let actionSheetController: UIAlertController = UIAlertController(title: "Alert", message: "Are you sure you want to cancel this post?", preferredStyle: .Alert)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
            //Do some stuff
        }
        actionSheetController.addAction(cancelAction)
        //Create and an option action
        let oKAction: UIAlertAction = UIAlertAction(title: "OK", style: .Default) { action -> Void in
            self.performSegueWithIdentifier("FinalCancel", sender: self)
        }
        actionSheetController.addAction(oKAction)
        //        //Add a text field
        //        actionSheetController.addTextFieldWithConfigurationHandler { textField -> Void in
        //            //TextField configuration
        //            textField.textColor = UIColor.blueColor()
        //        }
        //Present the AlertController
        self.presentViewController(actionSheetController, animated: true, completion: nil)
    }

    
    @IBAction func postIt(sender: AnyObject) {
        var interviewPost = PFObject(className: "InterviewPost")
        interviewPost.setObject(self.postObjectFinal.postTitle, forKey: "Title")
        interviewPost.setObject(self.postObjectFinal.postAddInfo, forKey: "Description")
        interviewPost.setObject(self.postObjectFinal.postSkills, forKey: "Skills")
        interviewPost.save()
//        interviewPost.saveInBackgroundWithBlock {
//            (success: Bool!, error: NSError!) -> Void in
//            if success {
//                NSLog("Object created with id: (interviewPost.objectId)")
//            } else {
//                NSLog("%@", error)
//            }
//        }
        self.tabBarController?.selectedIndex = 0
        
    }

    override func viewWillAppear(animated: Bool) {
        // navigationItem.title = &quot;One&quot;
        navigationItem.title = "I.A"
        
        var font = UIFont(name: "AvenirNext-DemiBold", size: 20)
        var barTintColor = UIColor(red: 209/255, green: 212/255, blue: 211/255, alpha: 1)
        var tintColor = UIColor(red: 0.906, green: 0.055, blue: 0.282, alpha: 1)
        navigationController?.navigationBar.barTintColor = barTintColor
        navigationController?.navigationBar.tintColor = tintColor
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.postFinalTitleText.text = self.postObjectFinal.postTitle
        self.postFinalDescText.text = self.postObjectFinal.postAddInfo
        self.postFinalSkillsText.text = self.postObjectFinal.postSkills
        
        self.navigationController?.navigationBar.tintColor = UIColor(red: 0.906, green: 0.055, blue: 0.282, alpha: 1)
        
        
        var labelsFont = "AvenirNext-Regular"
        var textViewsFont = "AvenirNext-UltraLightItalic"
        
        self.titleLabel.text = "Title"
        //self.titleLabel.textColor = UIColor(red: 17/255, green: 28/255, blue: 36/255, alpha: 1)
        self.titleLabel.font = UIFont(name: labelsFont, size:20)
        
        self.descriptionLabel.text = "Description"
     //   self.descriptionLabel.textColor = UIColor(red: 17/255, green: 28/255, blue: 36/255, alpha: 1)
        self.descriptionLabel.font = UIFont(name: labelsFont, size:20)
        
        self.skillsLabel.text = "Skill"
    //    self.skillsLabel.textColor = UIColor(red: 17/255, green: 28/255, blue: 36/255, alpha: 1)
        self.skillsLabel.font = UIFont(name: labelsFont, size:20)
        
        
        
        self.postFinalTitleText.font = UIFont(name: textViewsFont, size:18)
        self.postFinalTitleText.tintColor = UIColor.lightGrayColor()
        self.postFinalTitleText.layer.borderColor = UIColor(red: 209/255, green: 212/255, blue: 211/255, alpha: 1).CGColor
        self.postFinalTitleText.textColor = UIColor(red: 114/255, green: 104/255, blue: 91/255, alpha: 1)
        self.postFinalTitleText.layer.borderWidth = 1
        self.postFinalTitleText.layer.cornerRadius = 0.5
        
        self.postFinalDescText.font = UIFont(name: textViewsFont, size:18)
        self.postFinalDescText.tintColor = UIColor.lightGrayColor()
        self.postFinalDescText.layer.borderColor = UIColor(red: 209/255, green: 212/255, blue: 211/255, alpha: 1).CGColor
        self.postFinalDescText.textColor = UIColor(red: 114/255, green: 104/255, blue: 91/255, alpha: 1)
        self.postFinalDescText.layer.borderWidth = 1
        self.postFinalDescText.layer.cornerRadius = 0.5
        
        
        self.postFinalSkillsText.font = UIFont(name: textViewsFont, size:18)
        self.postFinalSkillsText.tintColor = UIColor.lightGrayColor()
        self.postFinalSkillsText.layer.borderColor = UIColor(red: 209/255, green: 212/255, blue: 211/255, alpha: 1).CGColor
        self.postFinalSkillsText.textColor = UIColor(red: 114/255, green: 104/255, blue: 91/255, alpha: 1)
        self.postFinalSkillsText.layer.borderWidth = 1
        self.postFinalSkillsText.layer.cornerRadius = 0.5
        
        self.postItFinalButton.backgroundColor=UIColor(red: 0.906, green: 0.055, blue: 0.282, alpha: 1)
        self.postItFinalButton.tintColor = UIColor.whiteColor()
        postItFinalButton.setTitle("POST IT", forState: UIControlState.Normal)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
